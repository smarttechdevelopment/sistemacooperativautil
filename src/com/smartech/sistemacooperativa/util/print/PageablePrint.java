package com.smartech.sistemacooperativa.util.print;

import java.awt.print.Pageable;
import javax.print.DocFlavor;

/**
 *
 * @author x3m
 */
public class PageablePrint extends AbstractPrint {

    private Pageable objPageable = null;

    public PageablePrint(Pageable objPageable) {
        this.objPageable = objPageable;
    }

    public static final void printPageable(Pageable objPageable, String printType) {
        new PageablePrint(objPageable).printPageable(printType);
    }

    public final void printPageable(String printType, String printer) {
        DocFlavor flavor = DocFlavor.SERVICE_FORMATTED.PAGEABLE;
        print(objPageable, flavor, printType, printer);
    }
    
    public final void printPageable(String printType) {
        this.printPageable(printType, null);
    }
}
