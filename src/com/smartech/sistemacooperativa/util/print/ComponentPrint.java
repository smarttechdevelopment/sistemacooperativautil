package com.smartech.sistemacooperativa.util.print;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import javax.swing.RepaintManager;

/**
 *
 * @author jazomand
 */
public class ComponentPrint extends AbstractPrint implements Printable {

    private final Component componentToBePrinted;

    public static void printComponent(Component c, String printType) {
        printComponent(c, printType, null);
    }
    public static void printComponent(Component c, String printType, String printer) {
        new ComponentPrint(c).printComponent(printType, printer);
    }

    public ComponentPrint(Component componentToBePrinted) {
        this.componentToBePrinted = componentToBePrinted;
    }

    public void printComponent(String printType) {
        printComponent(printType, null);
    }
    
    public void printComponent(String printType, String printer) {
        PrintablePrint.printPrintable(this, printType, printer);
    }

    @Override
    public int print(Graphics g, PageFormat pageFormat, int pageIndex) {
        /*
         * Version con soporte de Paginado:
         * http://www.developerdotstar.com/community/node/124
         * 
         * Codigo:
         * 

        int response = NO_SUCH_PAGE;

        Graphics2D g2 = (Graphics2D) g;

        //  for faster printing, turn off double buffering
        disableDoubleBuffering(componentToBePrinted);

        Dimension d = componentToBePrinted.getSize(); //get size of document
        double panelWidth = d.width; //width in pixels
        double panelHeight = d.height; //height in pixels

        double pageHeight = pf.getImageableHeight(); //height of printer page
        double pageWidth = pf.getImageableWidth(); //width of printer page

        double scale = pageWidth / panelWidth;
        int totalNumPages = (int) Math.ceil(scale * panelHeight / pageHeight);

        //  make sure not print empty pages
        if (pageIndex >= totalNumPages) {
            response = NO_SUCH_PAGE;
        }
        else {

            //  shift Graphic to line up with beginning of print-imageable region
            g2.translate(pf.getImageableX(), pf.getImageableY());
        
            //  shift Graphic to line up with beginning of next page to print
            g2.translate(0f, -pageIndex * pageHeight);
      
            //  scale the page so the width fits...
            g2.scale(scale, scale);

            componentToBePrinted.paint(g2); //repaint the page for printing

            enableDoubleBuffering(componentToBePrinted);
            response = Printable.PAGE_EXISTS;
         }
            return response;
         * 
         */
        if (pageIndex > 0) {
            return (NO_SUCH_PAGE);
        } else {
            Graphics2D g2d = (Graphics2D) g;
            g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
            disableDoubleBuffering(componentToBePrinted);
            componentToBePrinted.paint(g2d);
            enableDoubleBuffering(componentToBePrinted);
            return (PAGE_EXISTS);
        }
    }

    public static void disableDoubleBuffering(Component c) {
        RepaintManager currentManager = RepaintManager.currentManager(c);
        currentManager.setDoubleBufferingEnabled(false);
    }

    public static void enableDoubleBuffering(Component c) {
        RepaintManager currentManager = RepaintManager.currentManager(c);
        currentManager.setDoubleBufferingEnabled(true);
    }
}