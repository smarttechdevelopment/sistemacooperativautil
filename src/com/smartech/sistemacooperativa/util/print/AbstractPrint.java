package com.smartech.sistemacooperativa.util.print;

import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.MediaPrintableArea;
import javax.print.attribute.standard.PrintQuality;
import javax.print.attribute.standard.PrinterName;
import javax.print.attribute.standard.PrinterResolution;

abstract class AbstractPrint {

    protected final void print(Object data, DocFlavor flavor, String printType, String printer) {
        PrinterJob objPrinterJob = PrinterJob.getPrinterJob();
        objPrinterJob.setJobName("X3M Software");

        /*
         if (objPrinterJob.printDialog()) {
         PageFormat pPageFormat = objPrinterJob.defaultPage();
         Paper pPaper = pPageFormat.getPaper();
         pPaper.setImageableArea(1.0, 1.0, pPaper.getWidth() - 2.0, pPaper.getHeight() - 2.0);
         pPageFormat.setPaper(pPaper);
         pPageFormat = objPrinterJob.pageDialog(pPageFormat);
         Book pBook = new Book();
         pBook.append((Printable)data, pPageFormat);
         objPrinterJob.setPageable(pBook);
         try {
         objPrinterJob.print();
         } catch (PrinterException ex) {
         ex.printStackTrace();
         }
         }*/
        //PrintService pService = PrintServiceLookup.lookupDefaultPrintService();
        PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
        PrinterResolution pr
                = new PrinterResolution(240, 144, PrinterResolution.DPI);

        aset.add(pr);
        aset.add(PrintQuality.HIGH);

        PageFormat pPageFormat = objPrinterJob.defaultPage();
        Paper pPaper = pPageFormat.getPaper();
        pPaper.setImageableArea(0, 0, pPaper.getWidth(), pPaper.getHeight());
        pPageFormat.setPaper(pPaper);
        pPageFormat = objPrinterJob.validatePage(pPageFormat);

        if ((printer == null && objPrinterJob.printDialog(aset)) || (printer != null)) {
            if (printType.equalsIgnoreCase("swing")) {
                PrintService pService = objPrinterJob.getPrintService();
                if (printer != null) {
                    PrintServiceAttributeSet printServiceAttributeSet = new HashPrintServiceAttributeSet();
                    printServiceAttributeSet.add(new PrinterName(printer, null));
                    pService = PrintServiceLookup.lookupPrintServices(null, printServiceAttributeSet)[0];
                    
                    /*
                    int count = service.length;
                    DocPrintJob docPrintJob = null;
                    for (int i = 0; i < count; i++) {
                    if (service[i].getName().equalsIgnoreCase(printerNameDesired)) {
                    docPrintJob = service[i].createPrintJob();
                    i = count;
                    }
                    }
                     */
                }
                if (pService != null) {
                    DocPrintJob printJob = pService.createPrintJob();

                    HashPrintRequestAttributeSet asset = new HashPrintRequestAttributeSet();
                    //Printable objPrintable = (data instanceof Pageable ? ((Pageable) data).getPrintable(0) : (Printable) data);
                    asset.add(new MediaPrintableArea(0f, 0f, (float) pPaper.getWidth() * 72, (float) pPaper.getHeight() * 72, MediaPrintableArea.INCH));
                    //asset.add(new MediaPrintableArea(0f, 0f, 80, 1000, MediaPrintableArea.MM));
                    Doc doc = new SimpleDoc(data, flavor, null);

                    try {
                        printJob.print(doc, asset);
                    } catch (PrintException pe) {
                        if (pe.getMessage().indexOf("accepting job") != -1) {
                            // recommend prompting the user at this point if they want to force it
                            // so they'll know there may be a problem.
                            try {
                                // try printing again but ignore the not-accepting-jobs attribute
                                ForcedAcceptPrintService.setupPrintJob(printJob); // add secret ingredient
                                printJob.print(doc, asset);
                            } catch (PrintException e) {
                                System.out.println("Realmente no puedes imprimir, ni a la mala...");
                                e.printStackTrace();
                            }
                        } else {
                            System.out.println("No puedes imprimir...");
                            pe.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.println("No puedes imprimir, no tienes impresoras instaladas.");
                }
            } else if (printType.equalsIgnoreCase("awt")) {
                try {
                    pPageFormat = objPrinterJob.validatePage(pPageFormat);
                    if (flavor.getRepresentationClassName().equals(DocFlavor.SERVICE_FORMATTED.PAGEABLE.getRepresentationClassName())) {
                        Book pBook = new Book();
                        pBook.append((Printable) data, pPageFormat);
                        objPrinterJob.setPageable(pBook);
                        //objPrinterJob.setPageable((Pageable) data);
                    } else if (flavor.getRepresentationClassName().equals(DocFlavor.SERVICE_FORMATTED.PRINTABLE.getRepresentationClassName())) {
                        objPrinterJob.setPrintable((Printable) data, pPageFormat);
                    } else {
                        throw new Exception("El flavor que se quiso imprimir aun no es soportado por la impresion tipo AWT. Revisa el archivo de configuracion del software x3m.");
                    }
                    objPrinterJob.print();
                } catch (PrinterException pe) {
                    System.out.println("AWT Error printing: " + pe);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("No se ha implementado el tipo de impresion: " + printType + ". Revisa el archivo de configuracion del software x3m.");
            }
        }
    }
}