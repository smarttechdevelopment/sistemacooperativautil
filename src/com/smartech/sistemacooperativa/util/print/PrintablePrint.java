package com.smartech.sistemacooperativa.util.print;

import java.awt.print.Printable;
import javax.print.DocFlavor;

/**
 *
 * @author x3m
 */
public class PrintablePrint extends AbstractPrint{
    private Printable objPrintable = null;
    
    public final static void printPrintable(Printable objPrintable, String printType, String printer){
        new PrintablePrint(objPrintable).printPrintable(printType, printer);
    }
    
    public PrintablePrint(Printable objPrintable){
        this.objPrintable = objPrintable;
    }
    
    public final void printPrintable(String printType){
        this.printPrintable(printType, null);
    }
    
    public final void printPrintable(String printType, String printer){
        DocFlavor flavor = DocFlavor.SERVICE_FORMATTED.PRINTABLE;
        print(objPrintable, flavor, printType, printer);
    }
}
