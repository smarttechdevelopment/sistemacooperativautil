package com.smartech.sistemacooperativa.util.interfaces;

import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IObservable {

    public void addObserver(IObserver objIObserver);

    public List<IObserver> getObserverList();

    public void notifyObservers();
}
