package com.smartech.sistemacooperativa.util.interfaces;

/**
 *
 * @author Smartech
 */
public interface IObserver {

    public void update(IObservable objIObservable);
}
