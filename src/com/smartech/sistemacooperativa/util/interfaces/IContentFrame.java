/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.util.interfaces;

/**
 *
 * @author Smartech
 */
import javax.swing.JComponent;
import javax.swing.JFrame;

public interface IContentFrame {

    public JFrame getFrame();

    public JComponent getContentContainer();
}
