package com.smartech.sistemacooperativa.util.reporting;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.ResourceBundle;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import org.icepdf.ri.common.SwingController;
import org.icepdf.ri.common.SwingViewBuilder;
import org.icepdf.ri.common.views.DocumentViewControllerImpl;
import org.icepdf.ri.util.FontPropertiesManager;
import org.icepdf.ri.util.PropertiesManager;
import org.pentaho.reporting.engine.classic.core.MasterReport;
import org.pentaho.reporting.engine.classic.core.ReportProcessingException;
import org.pentaho.reporting.engine.classic.core.event.ReportProgressListener;
import org.pentaho.reporting.engine.classic.core.modules.output.pageable.base.PageableReportProcessor;
import org.pentaho.reporting.engine.classic.core.modules.output.pageable.graphics.PrintReportProcessor;
import org.pentaho.reporting.engine.classic.core.modules.output.pageable.pdf.PdfOutputProcessor;
import org.pentaho.reporting.engine.classic.core.modules.output.pageable.pdf.PdfReportUtil;
import org.pentaho.reporting.engine.classic.core.modules.output.pageable.plaintext.PlainTextReportUtil;
import org.pentaho.reporting.engine.classic.core.modules.output.table.csv.CSVReportUtil;
import org.pentaho.reporting.engine.classic.core.modules.output.table.html.HtmlReportUtil;
import org.pentaho.reporting.engine.classic.core.modules.output.table.rtf.RTFReportUtil;
import org.pentaho.reporting.engine.classic.core.modules.output.table.xls.ExcelReportUtil;
import com.smartech.sistemacooperativa.util.print.PageablePrint;

/**
 *
 * @author jazom
 */
class ICEpdfReportingPane extends JPanel {

    private boolean isMenuVisible = true;
    private SwingController controller = null;
    private MasterReport report = null;
    private ReportProgressListener progressListener = null;
    private byte[] pdf = new byte[0];

    public ICEpdfReportingPane(MasterReport report, ReportProgressListener progressListener, boolean isMenuVisible) {
        super(new BorderLayout(), true);

        this.report = report;
        this.progressListener = progressListener;
        this.isMenuVisible = isMenuVisible;

        // build a component controller
        controller = new SwingController();
        controller.setIsEmbeddedComponent(true);

        final PropertiesManager properties = new PropertiesManager(
                System.getProperties(),
                ResourceBundle.getBundle(PropertiesManager.DEFAULT_MESSAGE_BUNDLE));

        properties.set(PropertiesManager.PROPERTY_DEFAULT_ZOOM_LEVEL, "1.25");
        SwingViewBuilder factory = new SwingViewBuilder(controller, properties) {

            private JButton printJButton = null;

            private void setPrintButton(JButton jButton) {
                printJButton = jButton;
            }

            private JButton wrapperBuildPrintbutton() {
                JButton button = super.buildPrintButton();
                ActionListener listener = button.getActionListeners()[0];
                button.removeActionListener(listener);
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    PageablePrint.printPageable(new PrintReportProcessor(ICEpdfReportingPane.this.report), java.util.ResourceBundle.getBundle("com/smartech/sistemacooperativa/bll/config/DefaultConfig").getString("tipoImpresion"));
                                } catch (ReportProcessingException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });
                    }
                });
                return button;
            }

            @Override
            public JButton buildPrintButton() {
                try {
                    Runnable r = new Runnable() {
                        @Override
                        public void run() {
                            setPrintButton(wrapperBuildPrintbutton());
                        }
                    };
                    if (SwingUtilities.isEventDispatchThread()) {
                        r.run();
                    } else {
                        SwingUtilities.invokeAndWait(r);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return printJButton != null ? printJButton : super.buildPrintButton();
            }

            @Override
            public JToolBar buildCompleteToolBar(boolean embeddableComponent) {
                JToolBar toolBar = super.buildCompleteToolBar(embeddableComponent);
                //Quitar el boton de guardar PDF
                ((JToolBar) toolBar.getComponentAtIndex(0)).remove(0);
                toolBar.add(buildExportToolBar(), 0);
                return toolBar;
            }
        };

        // add interactive mouse link annotation support via callback
        controller.getDocumentViewController().setAnnotationCallback(
                new org.icepdf.ri.common.MyAnnotationCallback(controller.getDocumentViewController()));
        ICEpdfReportingPane.this.add(factory.buildViewerPanel(), BorderLayout.CENTER);
        if (ICEpdfReportingPane.this.isMenuVisible) {
            ICEpdfReportingPane.this.add(factory.buildCompleteMenuBar(), BorderLayout.NORTH);
        }

        // read/store the font cache.
        ResourceBundle messageBundle = ResourceBundle.getBundle(
                PropertiesManager.DEFAULT_MESSAGE_BUNDLE);
        new FontPropertiesManager(properties, System.getProperties(), messageBundle);

        new Thread(new Runnable() {
            @Override
            public void run() {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                PdfOutputProcessor processor = new PdfOutputProcessor(
                        ICEpdfReportingPane.this.report.getConfiguration(),
                        baos,
                        ICEpdfReportingPane.this.report.getResourceManager());
                try {
                    PageableReportProcessor prp = new PageableReportProcessor(ICEpdfReportingPane.this.report, processor);
                    if (ICEpdfReportingPane.this.progressListener != null) {
                        prp.addReportProgressListener(ICEpdfReportingPane.this.progressListener);
                    }
                    prp.processReport();
                    prp.close();
                    baos.flush();
                    pdf = baos.toByteArray();
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            controller.openDocument(new ByteArrayInputStream(pdf), null, null);
                            controller.setPageViewMode(DocumentViewControllerImpl.ONE_COLUMN_VIEW, true);
                        }
                    });
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public final void add(Component comp, Object constraints) {
        super.add(comp, constraints);
    }

    @Override
    protected final void finalize() throws Throwable {
        try {
            controller.closeDocument();
            controller.dispose();
        } finally {
            super.finalize();
        }
    }

    private JToolBar buildExportToolBar() {
        JToolBar toolBar = new JToolBar("Export");
        toolBar.add(buildExportJButton("PDF", "pdf", "pdf"));
        toolBar.add(buildExportJButton("Archivo de MS Excel", "xls", "xls"));
        toolBar.add(buildExportJButton("Archivo de MS Word", "rtf", "doc"));
        toolBar.add(buildExportJButton("Archivo separado por comas", "csv", "csv"));
        toolBar.add(buildExportJButton("Archivo de texto", "txt", "txt"));
        toolBar.add(buildExportJButton("Página web", "html", "htm"));
        toolBar.setFloatable(false);
        toolBar.addSeparator();
        return toolBar;
    }

    private JButton buildExportJButton(final String name, final String type, final String icon) {
        return buildJButton(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Date now = new Date();
                String filename = "reporte_" + forceDigits(now.getDate(), 2) + "-" + forceDigits(now.getMonth() + 1, 2) + "-" + (now.getYear() + 1900) + ".pdf";
                JFileChooser objJFileChooser = new JFileChooser(new File(filename));
                int state = objJFileChooser.showSaveDialog(ICEpdfReportingPane.this);
                if (state == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = objJFileChooser.getSelectedFile();
                    try {
                        String path = forceExtension(selectedFile.getAbsolutePath(), "." + type);
                        if (type.equalsIgnoreCase("pdf")) {
                            PdfReportUtil.createPDF(report, path);
                        }
                        if (type.equalsIgnoreCase("xls")) {
                            ExcelReportUtil.createXLS(report, path);
                        }
                        if (type.equalsIgnoreCase("rtf")) {
                            RTFReportUtil.createRTF(report, path);
                        }
                        if (type.equalsIgnoreCase("csv")) {
                            CSVReportUtil.createCSV(report, path);
                        }
                        if (type.equalsIgnoreCase("txt")) {
                            PlainTextReportUtil.createPlainText(report, path);
                        }
                        if (type.equalsIgnoreCase("html")) {
                            HtmlReportUtil.createDirectoryHTML(report, path);
                        }
                        if (Desktop.isDesktopSupported()) {
                            Desktop objDesktop = Desktop.getDesktop();
                            int confirm = JOptionPane.showConfirmDialog(ICEpdfReportingPane.this, "<html>Reporte guardado con &eacute;xito. <br/> &iquest;Deseas abrirlo?</html>", "Reporte", JOptionPane.YES_NO_OPTION);
                            if (confirm == JOptionPane.YES_OPTION) {
                                objDesktop.open(new File(path));
                            }
                        } else {
                            JOptionPane.showMessageDialog(ICEpdfReportingPane.this, "<html>Reporte guardado con &eacute;xito.</html>");
                        }
                    } catch (ReportProcessingException | IOException | HeadlessException ex) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(ICEpdfReportingPane.this, "El reporte no se pudo guardar.");
                    }
                }
                System.gc();
            }
        }, new ImageIcon(getClass().getResource("/com/smartech/sistemacooperativa/util/images/" + icon + ".png")), "Guardar como " + name);
    }

    private JButton buildJButton(ActionListener actionListener, Icon icon, String tooltip) {
        JButton button = new JButton(icon);
        button.addActionListener(actionListener);
        button.setToolTipText(tooltip);
        return button;
    }

    private String forceExtension(String absolutePath, String extension) {
        if (!absolutePath.endsWith(extension)) {
            absolutePath += extension;
        }
        return absolutePath;
    }

    private String forceDigits(int number, int digits) {
        String strNumber = number + "";
        while (strNumber.length() < digits) {
            strNumber = "0" + strNumber;
        }
        return strNumber;
    }
}
