package com.smartech.sistemacooperativa.util.reporting;

import com.smartech.sistemacooperativa.util.interfaces.IContentFrame;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JDialog;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.table.TableModel;
import org.pentaho.reporting.engine.classic.core.ClassicEngineBoot;
import org.pentaho.reporting.engine.classic.core.MasterReport;
import org.pentaho.reporting.engine.classic.core.ReportProcessingException;
import org.pentaho.reporting.engine.classic.core.TableDataFactory;
import org.pentaho.reporting.engine.classic.core.event.ReportProgressListener;
import org.pentaho.reporting.engine.classic.core.modules.output.pageable.graphics.PrintReportProcessor;
import org.pentaho.reporting.engine.classic.core.modules.parser.base.ReportGenerator;
import org.pentaho.reporting.engine.classic.core.parameters.DefaultParameterDefinition;
import org.pentaho.reporting.engine.classic.core.parameters.ParameterDefinitionEntry;
import org.pentaho.reporting.engine.classic.core.parameters.PlainParameter;
import org.pentaho.reporting.libraries.resourceloader.ResourceException;
import com.smartech.sistemacooperativa.util.print.PageablePrint;

/**
 *
 * @author Escritorio
 */
public class ReportUtil {

    private static String title = "X3M - Visor de Reportes";
    private static final ViewerType defaultViewerType = ViewerType.ICEpdf;

    public enum ViewerType {
        Native,
        ICEpdf
    }

    public static JPanel getReportPanel(URL in, TableModel data, ViewerType vType, ReportProgressListener progressListener) {
        return getReportPanel(in, data, new ArrayList<ParameterDefinitionEntry>(), vType, progressListener);
    }

    public static JPanel getReportPanel(URL in, Map<String, TableModel> data, ViewerType vType, ReportProgressListener progressListener) {
        return getReportPanel(in, data, new ArrayList<ParameterDefinitionEntry>(), vType, progressListener);
    }

    public static JPanel getReportPanel(URL in, TableModel data, List<ParameterDefinitionEntry> parameters) {
        return getReportPanel(in, data, parameters, defaultViewerType, null);
    }
    
    public static JPanel getReportPanel(URL in, TableModel data, List<ParameterDefinitionEntry> parameters, ViewerType vType, ReportProgressListener progressListener) {
        HashMap<String, TableModel> dataMap = new HashMap<>();
        dataMap.put("default", data);
        return getReportPanel(in, dataMap, parameters, vType, progressListener);
    }

    public static JPanel getReportPanel(URL in, Map<String, TableModel> data, List<ParameterDefinitionEntry> parameters, ViewerType vType, ReportProgressListener progressListener) {
        JPanel objPanel = null;
        try {
            ReportGenerator generator = ReportGenerator.getInstance();
            MasterReport report = generator.parseReport(in);
            DefaultParameterDefinition def = new DefaultParameterDefinition();
            for (ParameterDefinitionEntry objParameter : parameters) {
                def.addParameterDefinition(objParameter);
            }
            report.setParameterDefinition(def);
            TableDataFactory dataFactory = new TableDataFactory();
            for (String key : data.keySet()) {
                dataFactory.addTable(key, data.get(key));
            }
            report.setDataFactory(dataFactory);
            if (vType == ViewerType.Native) {
                objPanel = new X3MReportingPane(report);
            }else{
                objPanel = new ICEpdfReportingPane(report, progressListener, false);
            }
            
        } catch (IOException | ResourceException e) {
            e.printStackTrace();
        }
        return objPanel;
    }

    public static JInternalFrame getInternalReport(String title, URL in, TableModel data) {
        return getInternalReport(title, in, data, new ArrayList<ParameterDefinitionEntry>());
    }

    public static JInternalFrame getInternalReport(String title, URL in, TableModel data, List<ParameterDefinitionEntry> parameters) {
        JInternalFrame objJInternalFrame = new JInternalFrame(title);
        objJInternalFrame.getContentPane().add(getReportPanel(in, data, parameters));
        objJInternalFrame.pack();
        return objJInternalFrame;
    }

    public static void showReport(IContentFrame objContentFrame, URL in, TableModel data) {
        showReport(objContentFrame, in, data, true);
    }

    public static void showReport(IContentFrame objContentFrame, URL in, TableModel data, boolean modal) {
        showReport(objContentFrame, in, data, new ArrayList<ParameterDefinitionEntry>(), modal);
    }

    public static void showReport(IContentFrame objContentFrame, URL in, HashMap<String, TableModel> data) {
        showReport(objContentFrame, in, data, true);
    }

    public static void showReport(IContentFrame objContentFrame, URL in, HashMap<String, TableModel> data, boolean modal) {
        showReport(objContentFrame, in, data, new ArrayList<ParameterDefinitionEntry>(), modal);
    }

    public static void showReport(IContentFrame objContentFrame, URL in, TableModel data, List<ParameterDefinitionEntry> parameters) {
        showReport(objContentFrame, in, data, parameters, true);
    }

    public static void showReport(IContentFrame objContentFrame, URL in, TableModel data, List<ParameterDefinitionEntry> parameters, boolean modal) {
        HashMap<String, TableModel> dataMap = new HashMap<>();
        dataMap.put("default", data);
        showReport(objContentFrame, in, dataMap, parameters, modal);
    }

    public static void showReport(IContentFrame objContentFrame, URL in, HashMap<String, TableModel> data, List<ParameterDefinitionEntry> parameters) {
        showReport(objContentFrame, in, data, parameters, true);
    }

    public static void showReport(IContentFrame objContentFrame, URL in, HashMap<String, TableModel> data, List<ParameterDefinitionEntry> parameters, boolean modal) {
        try {
            JPanel reportPanel = getReportPanel(in, data, parameters, defaultViewerType, null);
            JDialog objDialog = new JDialog(objContentFrame.getFrame(), title, modal);
            objDialog.getContentPane().add(reportPanel);
            objDialog.pack();
            objDialog.setBounds(objContentFrame.getFrame().getBounds());
            objDialog.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void printReport(URL in, TableModel data, String printType) {
        try {
            final ReportGenerator generator = ReportGenerator.getInstance();
            MasterReport report = generator.parseReport(in);
            report.setDataFactory(new TableDataFactory("default", data));
            //PrintUtil.print(report);
            PageablePrint.printPageable(new PrintReportProcessor(report), printType);
        } catch (IOException | ResourceException | ReportProcessingException e) {
            e.printStackTrace();
        }
    }

    public static void startEngine() {
        ClassicEngineBoot.getInstance().start();
    }

    public static ParameterDefinitionEntry newParameter(String key, String value) {
        PlainParameter parameter = new PlainParameter(key);
        parameter.setDefaultValue(value);
        return parameter;
    }
    
    public static HashMap<String, TableModel> getDataOnly(Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> reportData){
        for(Map.Entry<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> entry : reportData.entrySet()){
            return entry.getKey();
        }
        
        return null;
    }
    
    public static List<ParameterDefinitionEntry> getParametersOnly(Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> reportData){
        for(Map.Entry<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> entry : reportData.entrySet()){
            return entry.getValue();
        }
        
        return null;
    }

    /**
     * @return the title
     */
    public static String getTitle() {
        return title;
    }

    /**
     * @param aTitle the title to set
     */
    public static void setTitle(String aTitle) {
        title = aTitle;
    }
}
