package com.smartech.sistemacooperativa.util.reporting;

import javax.swing.SwingUtilities;
import org.pentaho.reporting.engine.classic.core.event.ReportProgressEvent;
import org.pentaho.reporting.engine.classic.core.event.ReportProgressListener;

/**
 *
 * @author jazom
 */
public abstract class SwingProgressListener implements ReportProgressListener{

    public abstract void swingReportProcessingStarted(ReportProgressEvent rpe);
    public abstract void swingReportProcessingUpdate(ReportProgressEvent rpe);
    public abstract void swingReportProcessingFinished(ReportProgressEvent rpe);
    
    @Override
    public void reportProcessingStarted(final ReportProgressEvent rpe) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                swingReportProcessingStarted(rpe);
            }
        });
    }

    @Override
    public void reportProcessingUpdate(final ReportProgressEvent rpe) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                swingReportProcessingUpdate(rpe);
            }
        });
    }

    @Override
    public void reportProcessingFinished(final ReportProgressEvent rpe) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                swingReportProcessingFinished(rpe);
            }
        });
    }
    
}