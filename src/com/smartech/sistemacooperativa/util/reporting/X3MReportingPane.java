/*
 * X3MReportingPane.java
 *
 * Created on 27/12/2010, 09:45:33 PM
 */
package com.smartech.sistemacooperativa.util.reporting;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.HeadlessException;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import javax.swing.*;
import org.pentaho.reporting.engine.classic.core.MasterReport;
import org.pentaho.reporting.engine.classic.core.ReportProcessingException;
import org.pentaho.reporting.engine.classic.core.modules.gui.base.PreviewPane;
import org.pentaho.reporting.engine.classic.core.modules.gui.base.SwingPreviewModule;
import org.pentaho.reporting.engine.classic.core.modules.gui.commonswing.*;
import org.pentaho.reporting.engine.classic.core.modules.output.pageable.pdf.PdfReportUtil;
import org.pentaho.reporting.engine.classic.core.modules.output.pageable.plaintext.PlainTextReportUtil;
import org.pentaho.reporting.engine.classic.core.modules.output.table.csv.CSVReportUtil;
import org.pentaho.reporting.engine.classic.core.modules.output.table.html.HtmlReportUtil;
import org.pentaho.reporting.engine.classic.core.modules.output.table.rtf.RTFReportUtil;
import org.pentaho.reporting.engine.classic.core.modules.output.table.xls.ExcelReportUtil;
import org.pentaho.reporting.libraries.base.util.Messages;


/**
 * 
 * @author Jazo
 */
public class X3MReportingPane extends javax.swing.JPanel {

    private final double zoom = 1d;
    private final boolean progressBarEnabled = true;
    private final boolean progressDialogEnabled = true;
    private MasterReport currentReport = null;
    //Pentaho GUI Controls
    private final JStatusBar objStatusBar;
    private final ReportProgressBar objProgressBar;
    private final ReportProgressDialog objProgressDialog;
    private final PreviewPane objPreviewPane;
    private final JLabel objPageLabel;
    private final Messages objMessages;

    /** Creates new form X3MReportingPane
     * @param report */
    X3MReportingPane(MasterReport report) {
        initComponents();
        ReportUtil.startEngine();

        currentReport = report;

        addComponentListener(new RequestFocusHandler());
        objMessages = new Messages(getLocale(), SwingPreviewModule.BUNDLE_NAME);

        objPreviewPane = new PreviewPane();
        objPreviewPane.setDeferredRepagination(true);

        objStatusBar = new JStatusBar(objPreviewPane.getIconTheme());
        objPageLabel = new JLabel();

        if (progressBarEnabled) {
            objProgressBar = new ReportProgressBar();
            objProgressBar.setVisible(false);
            objPreviewPane.addReportProgressListener(objProgressBar);
            objPreviewPane.addPropertyChangeListener(new PreviewPanePropertyChangeHandler());
        } else {
            objProgressBar = null;
        }

        if (progressDialogEnabled) {
            objProgressDialog = new ReportProgressDialog();
            final MasterReport reportJob = currentReport;
            if (reportJob == null || reportJob.getTitle() == null) {
                objProgressDialog.setTitle(objMessages.getString("ProgressDialog.EMPTY_TITLE"));
                objProgressDialog.setMessage(objMessages.getString("ProgressDialog.EMPTY_TITLE"));
            } else {
                objProgressDialog.setTitle(objMessages.getString("ProgressDialog.TITLE", reportJob.getTitle()));
                objProgressDialog.setMessage(objMessages.getString("ProgressDialog.TITLE", reportJob.getTitle()));
            }
            objProgressDialog.pack();
        } else {
            objProgressDialog = null;
        }

        final JComponent extensionArea = objStatusBar.getExtensionArea();
        extensionArea.setLayout(new BoxLayout(extensionArea, BoxLayout.X_AXIS));
        if (objProgressBar != null) {
            extensionArea.add(objProgressBar);
        }
        extensionArea.add(objPageLabel);

        final JComponent contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(objPreviewPane, BorderLayout.CENTER);
        contentPane.add(objStatusBar, BorderLayout.SOUTH);

        pnlMain.removeAll();
        pnlMain.setLayout(new BorderLayout());
        pnlMain.add(contentPane, BorderLayout.CENTER);
        pnlMain.validate();

        objStatusBar.setIconTheme(objPreviewPane.getIconTheme());
        objStatusBar.setStatus(objPreviewPane.getStatusType(), objPreviewPane.getStatusText());

        objPreviewPane.setReportJob(currentReport);
        objPreviewPane.setZoom(zoom);

        if (objPreviewPane.getReportController() != null && objPreviewPane.getReportController().getControlPanel() != null) {
            objPreviewPane.getReportController().getControlPanel().setVisible(false);
        }

        objPreviewPane.startPagination();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        btnPDF = new javax.swing.JButton();
        btnXLS = new javax.swing.JButton();
        btnRTF = new javax.swing.JButton();
        btnCSV = new javax.swing.JButton();
        btnTXT = new javax.swing.JButton();
        btnHTML = new javax.swing.JButton();
        pnlMain = new javax.swing.JPanel();
        jProgressBar1 = new javax.swing.JProgressBar();
        jLabel1 = new javax.swing.JLabel();

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);
        jToolBar1.setMargin(new java.awt.Insets(5, 0, 0, 0));
        jToolBar1.setMaximumSize(new java.awt.Dimension(79, 49));
        jToolBar1.setMinimumSize(new java.awt.Dimension(79, 49));

        btnPDF.setIcon(new javax.swing.ImageIcon(getClass().getResource("/x3m/util/images/pdf.png"))); // NOI18N
        btnPDF.setToolTipText("Guardar como PDF");
        btnPDF.setFocusable(false);
        btnPDF.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPDF.setMaximumSize(new java.awt.Dimension(35, 35));
        btnPDF.setMinimumSize(new java.awt.Dimension(35, 35));
        btnPDF.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnPDF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPDFActionPerformed(evt);
            }
        });
        jToolBar1.add(btnPDF);

        btnXLS.setIcon(new javax.swing.ImageIcon(getClass().getResource("/x3m/util/images/xls.png"))); // NOI18N
        btnXLS.setToolTipText("Guardar como XLS");
        btnXLS.setFocusable(false);
        btnXLS.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnXLS.setMaximumSize(new java.awt.Dimension(35, 35));
        btnXLS.setMinimumSize(new java.awt.Dimension(35, 35));
        btnXLS.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnXLS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXLSActionPerformed(evt);
            }
        });
        jToolBar1.add(btnXLS);

        btnRTF.setIcon(new javax.swing.ImageIcon(getClass().getResource("/x3m/util/images/doc.png"))); // NOI18N
        btnRTF.setToolTipText("Guardar como RTF");
        btnRTF.setFocusable(false);
        btnRTF.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRTF.setMaximumSize(new java.awt.Dimension(35, 35));
        btnRTF.setMinimumSize(new java.awt.Dimension(35, 35));
        btnRTF.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnRTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRTFActionPerformed(evt);
            }
        });
        jToolBar1.add(btnRTF);

        btnCSV.setIcon(new javax.swing.ImageIcon(getClass().getResource("/x3m/util/images/csv.png"))); // NOI18N
        btnCSV.setToolTipText("Guardar como CSV");
        btnCSV.setFocusable(false);
        btnCSV.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCSV.setMaximumSize(new java.awt.Dimension(35, 35));
        btnCSV.setMinimumSize(new java.awt.Dimension(35, 35));
        btnCSV.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCSV.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCSVActionPerformed(evt);
            }
        });
        jToolBar1.add(btnCSV);

        btnTXT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/x3m/util/images/txt.png"))); // NOI18N
        btnTXT.setToolTipText("Guardar como TXT");
        btnTXT.setFocusable(false);
        btnTXT.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnTXT.setMaximumSize(new java.awt.Dimension(35, 35));
        btnTXT.setMinimumSize(new java.awt.Dimension(35, 35));
        btnTXT.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnTXT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTXTActionPerformed(evt);
            }
        });
        jToolBar1.add(btnTXT);

        btnHTML.setIcon(new javax.swing.ImageIcon(getClass().getResource("/x3m/util/images/htm.png"))); // NOI18N
        btnHTML.setToolTipText("Guardar como HTML");
        btnHTML.setFocusable(false);
        btnHTML.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnHTML.setMaximumSize(new java.awt.Dimension(35, 35));
        btnHTML.setMinimumSize(new java.awt.Dimension(35, 35));
        btnHTML.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnHTML.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHTMLActionPerformed(evt);
            }
        });
        jToolBar1.add(btnHTML);

        jProgressBar1.setIndeterminate(true);

        jLabel1.setText("Cargando...");

        javax.swing.GroupLayout pnlMainLayout = new javax.swing.GroupLayout(pnlMain);
        pnlMain.setLayout(pnlMainLayout);
        pnlMainLayout.setHorizontalGroup(
            pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMainLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jProgressBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 516, Short.MAX_VALUE)
                    .addComponent(jLabel1))
                .addContainerGap())
        );
        pnlMainLayout.setVerticalGroup(
            pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMainLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(329, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 540, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnPDFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPDFActionPerformed
        Date now = new Date();
        String filename = "reporte_" + forceDigits(now.getDate(), 2) + "-" + forceDigits(now.getMonth() + 1, 2) + "-" + (now.getYear() + 1900) + ".pdf";
        JFileChooser objJFileChooser = new JFileChooser(new File(filename));
        int state = objJFileChooser.showSaveDialog(this);
        if (state == JFileChooser.APPROVE_OPTION) {
            File selectedFile = objJFileChooser.getSelectedFile();
            try {
                String path = forceExtension(selectedFile.getAbsolutePath(), ".pdf");
                PdfReportUtil.createPDF(currentReport, path);
                if (Desktop.isDesktopSupported()) {
                    Desktop objDesktop = Desktop.getDesktop();
                    int confirm = JOptionPane.showConfirmDialog(this, "<html>Reporte guardado con &eacute;xito. <br/> &iquest;Deseas abrirlo?</html>", "Reporte", JOptionPane.YES_NO_OPTION);
                    if (confirm == JOptionPane.YES_OPTION) {
                        objDesktop.open(new File(path));
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "<html>Reporte guardado con &eacute;xito.</html>");
                }
            } catch (ReportProcessingException | IOException | HeadlessException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(this, "El reporte no se pudo guardar.");
            }
        }
        System.gc();
    }//GEN-LAST:event_btnPDFActionPerformed

    private void btnXLSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXLSActionPerformed
        Date now = new Date();
        String filename = "reporte_" + forceDigits(now.getDate(), 2) + "-" + forceDigits(now.getMonth() + 1, 2) + "-" + (now.getYear() + 1900) + ".xls";
        JFileChooser objJFileChooser = new JFileChooser(new File(filename));
        int state = objJFileChooser.showSaveDialog(this);
        if (state == JFileChooser.APPROVE_OPTION) {
            File selectedFile = objJFileChooser.getSelectedFile();
            try {
                String path = forceExtension(selectedFile.getAbsolutePath(), ".xls");
                ExcelReportUtil.createXLS(currentReport, path);
                if (Desktop.isDesktopSupported()) {
                    Desktop objDesktop = Desktop.getDesktop();
                    int confirm = JOptionPane.showConfirmDialog(this, "<html>Reporte guardado con &eacute;xito. <br/> &iquest;Deseas abrirlo?</html>", "Reporte", JOptionPane.YES_NO_OPTION);
                    if (confirm == JOptionPane.YES_OPTION) {
                        objDesktop.open(new File(path));
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "<html>Reporte guardado con &eacute;xito.</html>");
                }
            } catch (IOException | ReportProcessingException | HeadlessException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(this, "El reporte no se pudo guardar.");
            }
        }
        System.gc();
    }//GEN-LAST:event_btnXLSActionPerformed

    private void btnRTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRTFActionPerformed
        Date now = new Date();
        String filename = "reporte_" + forceDigits(now.getDate(), 2) + "-" + forceDigits(now.getMonth() + 1, 2) + "-" + (now.getYear() + 1900) + ".rtf";
        JFileChooser objJFileChooser = new JFileChooser(new File(filename));
        int state = objJFileChooser.showSaveDialog(this);
        if (state == JFileChooser.APPROVE_OPTION) {
            File selectedFile = objJFileChooser.getSelectedFile();
            try {
                String path = forceExtension(selectedFile.getAbsolutePath(), ".rtf");
                RTFReportUtil.createRTF(currentReport, path);
                if (Desktop.isDesktopSupported()) {
                    Desktop objDesktop = Desktop.getDesktop();
                    int confirm = JOptionPane.showConfirmDialog(this, "<html>Reporte guardado con &eacute;xito. <br/> &iquest;Deseas abrirlo?</html>", "Reporte", JOptionPane.YES_NO_OPTION);
                    if (confirm == JOptionPane.YES_OPTION) {
                        objDesktop.open(new File(path));
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "<html>Reporte guardado con &eacute;xito.</html>");
                }
            } catch (IOException | ReportProcessingException | HeadlessException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(this, "El reporte no se pudo guardar.");
            }
        }
        System.gc();
    }//GEN-LAST:event_btnRTFActionPerformed

    private void btnCSVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCSVActionPerformed
        Date now = new Date();
        String filename = "reporte_" + forceDigits(now.getDate(), 2) + "-" + forceDigits(now.getMonth() + 1, 2) + "-" + (now.getYear() + 1900) + ".csv";
        JFileChooser objJFileChooser = new JFileChooser(new File(filename));
        int state = objJFileChooser.showSaveDialog(this);
        if (state == JFileChooser.APPROVE_OPTION) {
            File selectedFile = objJFileChooser.getSelectedFile();
            try {
                String path = forceExtension(selectedFile.getAbsolutePath(), ".csv");
                CSVReportUtil.createCSV(currentReport, path);
                if (Desktop.isDesktopSupported()) {
                    Desktop objDesktop = Desktop.getDesktop();
                    int confirm = JOptionPane.showConfirmDialog(this, "<html>Reporte guardado con &eacute;xito. <br/> &iquest;Deseas abrirlo?</html>", "Reporte", JOptionPane.YES_NO_OPTION);
                    if (confirm == JOptionPane.YES_OPTION) {
                        objDesktop.open(new File(path));
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "<html>Reporte guardado con &eacute;xito.</html>");
                }
            } catch (ReportProcessingException | IOException | HeadlessException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(this, "El reporte no se pudo guardar.");
            }
        }
        System.gc();
    }//GEN-LAST:event_btnCSVActionPerformed

    private void btnTXTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTXTActionPerformed
        Date now = new Date();
        String filename = "reporte_" + forceDigits(now.getDate(), 2) + "-" + forceDigits(now.getMonth() + 1, 2) + "-" + (now.getYear() + 1900) + ".txt";
        JFileChooser objJFileChooser = new JFileChooser(new File(filename));
        int state = objJFileChooser.showSaveDialog(this);
        if (state == JFileChooser.APPROVE_OPTION) {
            File selectedFile = objJFileChooser.getSelectedFile();
            try {
                String path = forceExtension(selectedFile.getAbsolutePath(), ".txt");
                PlainTextReportUtil.createPlainText(currentReport, path);
                if (Desktop.isDesktopSupported()) {
                    Desktop objDesktop = Desktop.getDesktop();
                    int confirm = JOptionPane.showConfirmDialog(this, "<html>Reporte guardado con &eacute;xito. <br/> &iquest;Deseas abrirlo?</html>", "Reporte", JOptionPane.YES_NO_OPTION);
                    if (confirm == JOptionPane.YES_OPTION) {
                        objDesktop.open(new File(path));
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "<html>Reporte guardado con &eacute;xito.</html>");
                }
            } catch (IOException | ReportProcessingException | HeadlessException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(this, "El reporte no se pudo guardar.");
            }
        }
        System.gc();
    }//GEN-LAST:event_btnTXTActionPerformed

    private void btnHTMLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHTMLActionPerformed
        Date now = new Date();
        String filename = "reporte_" + forceDigits(now.getDate(), 2) + "-" + forceDigits(now.getMonth() + 1, 2) + "-" + (now.getYear() + 1900) + ".html";
        JFileChooser objJFileChooser = new JFileChooser(new File(filename));
        int state = objJFileChooser.showSaveDialog(this);
        if (state == JFileChooser.APPROVE_OPTION) {
            File selectedFile = objJFileChooser.getSelectedFile();
            try {
                String path = forceExtension(selectedFile.getAbsolutePath(), ".html");
                HtmlReportUtil.createStreamHTML(currentReport, path);
                if (Desktop.isDesktopSupported()) {
                    Desktop objDesktop = Desktop.getDesktop();
                    int confirm = JOptionPane.showConfirmDialog(this, "<html>Reporte guardado con &eacute;xito. <br/> &iquest;Deseas abrirlo?</html>", "Reporte", JOptionPane.YES_NO_OPTION);
                    if (confirm == JOptionPane.YES_OPTION) {
                        objDesktop.open(new File(path));
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "<html>Reporte guardado con &eacute;xito.</html>");
                }
            } catch (IOException | ReportProcessingException | HeadlessException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(this, "El reporte no se pudo guardar.");
            }
        }
        System.gc();
    }//GEN-LAST:event_btnHTMLActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCSV;
    private javax.swing.JButton btnHTML;
    private javax.swing.JButton btnPDF;
    private javax.swing.JButton btnRTF;
    private javax.swing.JButton btnTXT;
    private javax.swing.JButton btnXLS;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JPanel pnlMain;
    // End of variables declaration//GEN-END:variables

    private class PreviewPanePropertyChangeHandler implements PropertyChangeListener {

        protected PreviewPanePropertyChangeHandler() {
        }

        /**
         * This method gets called when a bound property is changed.
         *
         * @param evt A PropertyChangeEvent object describing the event source and the property that has changed.
         */
        @Override
        public void propertyChange(final PropertyChangeEvent evt) {
            final String propertyName = evt.getPropertyName();
            final PreviewPane previewPane = objPreviewPane;
            final JStatusBar statusBar = objStatusBar;
            if (PreviewPane.MENU_PROPERTY.equals(propertyName)) {
                // Update the menu
                //final JMenu[] menus = previewPane.getMenu();
                //updateMenu(menus);
                return;
            }

            if (PreviewPane.TITLE_PROPERTY.equals(propertyName)) {
                //setTitle(previewPane.getTitle());
                return;
            }

            if (PreviewPane.STATUS_TEXT_PROPERTY.equals(propertyName) || PreviewPane.STATUS_TYPE_PROPERTY.equals(propertyName)) {
                statusBar.setStatus(previewPane.getStatusType(), previewPane.getStatusText());
                return;
            }

            if (PreviewPane.ICON_THEME_PROPERTY.equals(propertyName)) {
                statusBar.setIconTheme(previewPane.getIconTheme());
                return;
            }

            if (PreviewPane.PAGINATING_PROPERTY.equals(propertyName)) {
                if (Boolean.TRUE.equals(evt.getNewValue())) {
                    objPageLabel.setVisible(false);
                    //statusBar.setStatus(StatusType.INFORMATION, objMessages.getString("PreviewDialog.USER_PAGINATING")); //$NON-NLS-1$
                    if (objProgressBar != null) {
                        previewPane.addReportProgressListener(objProgressBar);
                        objProgressBar.setOnlyPagination(true);
                        objProgressBar.setVisible(true);
                        objProgressBar.revalidate();
                    }
                    if (objProgressDialog != null) {
                        previewPane.addReportProgressListener(objProgressDialog);
                        SwingUtil.centerDialogInParent(objProgressDialog);
                        objProgressDialog.setOnlyPagination(true);
                        objProgressDialog.setVisible(true);
                    }
                } else {
                    objPageLabel.setVisible(true);
                    //statusBar.setStatus(StatusType.NONE, ""); //$NON-NLS-1$
                    if (objProgressBar != null) {
                        objProgressBar.setOnlyPagination(false);
                        objProgressBar.setVisible(false);
                        previewPane.removeReportProgressListener(objProgressBar);
                        objProgressBar.revalidate();
                    }
                    if (objProgressDialog != null) {
                        previewPane.removeReportProgressListener(objProgressDialog);
                        objProgressDialog.setOnlyPagination(false);
                        objProgressDialog.setVisible(false);
                    }
                }
                return;
            }

            if (PreviewPane.PAGE_NUMBER_PROPERTY.equals(propertyName) || PreviewPane.NUMBER_OF_PAGES_PROPERTY.equals(propertyName)) {
                objPageLabel.setText(previewPane.getPageNumber() + "/" + previewPane.getNumberOfPages()); //$NON-NLS-1$
            }
        }
    }

    private String getFilenameFromPath(String path) {
        String[] split1 = path.split("/");
        String[] split2 = split1[split1.length - 1].split("\\\\");
        return split2[split2.length - 1];
    }

    private String removeExtension(String filename) {
        String[] split = filename.split("\\.");
        String retorno = split[0];
        for (int i = 1; i < split.length - 1; i++) {
            retorno += "." + split[i];
        }
        return retorno;
    }

    private String forceExtension(String absolutePath, String extension) {
        if (!absolutePath.endsWith(extension)) {
            absolutePath += extension;
        }
        return absolutePath;
    }

    private String forceDigits(int number, int digits) {
        String strNumber = number + "";
        while (strNumber.length() < digits) {
            strNumber = "0" + strNumber;
        }
        return strNumber;
    }
}