package com.smartech.sistemacooperativa.util.generics;

import java.math.BigDecimal;

/**
 *
 * @author Smartech
 */
public class BigDecimalUtil {
    
    private BigDecimalUtil(){
    }
    
    public static BigDecimal round(BigDecimal number){
        return round(number, 2);
    }
    
    public static BigDecimal round(BigDecimal number, int decimals){
        return number.divide(BigDecimal.ONE, decimals, BigDecimal.ROUND_HALF_UP);
    }
    
    public static String formatMiles(BigDecimal number){
        //falta corregir... no se usa
        String numero = BigDecimalUtil.round(number).toPlainString();
        String[] array = numero.split("\\.");
        String[] partes = new String[array[0].length()/3];
        String temp = "";
        int max = array[0].length();
        int p = 0;
        int c = 0;
        for(int i = max-1; i >= 0; i--){
            c++;
            temp = temp.concat(String.valueOf(array[0].charAt(i)));
            if(c%3 == 0){
                partes[p] = temp;
                p++;
                temp = "";
            }
        }
        
        String numeroFinal = "";
        for(String s : partes){
            numeroFinal = numeroFinal.concat("'" + s);
        }
        
        numeroFinal = numeroFinal.concat("." + array[1]);
        
        return numeroFinal;
    }
}
