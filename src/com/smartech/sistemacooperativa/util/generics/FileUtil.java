package com.smartech.sistemacooperativa.util.generics;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;
import java.util.List;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

/**
 *
 * @author Smartech
 */
public class FileUtil {

    private static final FTPClient connection = new FTPClient();
    //private static boolean isConnected;

    private FileUtil() {
    }

    public static void connect() {
        try {
            if (!connection.isConnected()) {
                String server = java.util.ResourceBundle.getBundle("com/smartech/sistemacooperativa/connection/config/FTPServerConnection").getString("ftpServer");
                int port = Integer.parseInt(java.util.ResourceBundle.getBundle("com/smartech/sistemacooperativa/connection/config/FTPServerConnection").getString("ftpPort"));
                String user = java.util.ResourceBundle.getBundle("com/smartech/sistemacooperativa/connection/config/FTPServerConnection").getString("ftpUser");
                String pass = java.util.ResourceBundle.getBundle("com/smartech/sistemacooperativa/connection/config/FTPServerConnection").getString("ftpPass");

                connection.connect(server, port);
                showServerReply(connection);
                int replyCode = connection.getReplyCode();
                if (!FTPReply.isPositiveCompletion(replyCode)) {
                    System.out.println("Operation failed. Server reply code: " + replyCode);
                    return;
                }
                boolean success = connection.login(user, pass);
                showServerReply(connection);
                if (!success) {
                    System.out.println("Could not login to the server");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void disconnect() {
        try {
            if (connection.isConnected()) {
                connection.logout();
                connection.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void showServerReply(FTPClient ftpClient) {
        String[] replies = ftpClient.getReplyStrings();
        if (replies != null && replies.length > 0) {
            for (String reply : replies) {
                System.out.println("SERVER: " + reply);
            }
        }
    }

//    private static FTPClient connectServer(String server, int port, String user, String pass) {
//        FTPClient ftpClient = null;
//
//        try {
//            ftpClient = new FTPClient();
//            ftpClient.connect(server, port);
//            showServerReply(ftpClient);
//            int replyCode = ftpClient.getReplyCode();
//            if (!FTPReply.isPositiveCompletion(replyCode)) {
//                System.out.println("Operation failed. Server reply code: " + replyCode);
//                return null;
//            }
//            boolean success = ftpClient.login(user, pass);
//            showServerReply(ftpClient);
//            if (!success) {
//                isConnected = false;
//                System.out.println("Could not login to the server");
//                return null;
//            }
//
//            isConnected = true;
//        } catch (Exception e) {
//            isConnected = false;
//            e.printStackTrace();
//        }
//
//        return ftpClient;
//    }
    private static boolean checkDirectoryExists(FTPClient ftpClient, String dirPath) {
        boolean result = false;

        try {
            ftpClient.changeWorkingDirectory(dirPath);
            int returnCode = ftpClient.getReplyCode();
            return returnCode != 550;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public static BufferedImage downloadImageFTP(String path) {
        if (connection.isConnected()) {
            if (path != null) {
                if (!path.isEmpty()) {
                    //FTPClient ftpClient = connectServer(server, port, user, pass);

                    try {
                        File downloadFile = new File("temp." + FileUtil.getExtension(path));

                        connection.enterLocalPassiveMode();
                        connection.setFileType(FTP.BINARY_FILE_TYPE);
                        connection.setFileTransferMode(FTP.BINARY_FILE_TYPE);

                        OutputStream output = new FileOutputStream(downloadFile.getAbsolutePath());
                        connection.retrieveFile(path, output);
                        output.close();

                        System.out.println("File " + path + " has been downloaded successfully.");

                        return ImageIO.read(downloadFile);
                    } catch (Exception e) {
                        e.printStackTrace();
//                    } finally {
//                        try {
//                            ftpClient.logout();
//                            ftpClient.disconnect();
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
                    }
                }
            }
        } else {
            System.out.println("Not connected to FTP");
        }

        return null;
    }

    public static InputStream downloadFileFTP(String path) {
        if (connection.isConnected()) {
            if (path != null) {
                if (!path.isEmpty()) {
                    //FTPClient ftpClient = connectServer(server, port, user, pass);
                    try {
                        File downloadFile = new File("doctemp." + FileUtil.getExtension(path));

                        connection.enterLocalPassiveMode();
                        connection.setFileType(FTP.BINARY_FILE_TYPE);
                        connection.setFileTransferMode(FTP.BINARY_FILE_TYPE);

                        OutputStream output = new FileOutputStream(downloadFile.getAbsolutePath());
                        connection.retrieveFile(path, output);
                        output.close();

                        System.out.println("File " + path + " has been downloaded successfully.");

                        FileInputStream fileInputStream = new FileInputStream(downloadFile);
                        return fileInputStream;
                    } catch (FileNotFoundException e) {
                        System.out.println("File " + path + " not found.");
                    } catch (Exception e) {
                        e.printStackTrace();
//                } finally {
//                    try {
//                        ftpClient.logout();
//                        ftpClient.disconnect();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                    }
                }
            }
        } else {
            System.out.println("Not connected to FTP");
        }

        return null;
    }

    public static void deleteTempFile(String path) {
        File tempFile = new File("temp." + FileUtil.getExtension(path));
        tempFile.delete();
    }

    public static boolean createDirectoryFTP(String directory) {
        boolean result = false;
        if (connection.isConnected()) {
            try {
                //FTPClient ftpClient = connectServer(server, port, user, pass);
//            if (connection != null) {
                if (!checkDirectoryExists(connection, directory)) {
                    result = connection.makeDirectory(directory);
                    showServerReply(connection);
                    if (result) {
                        System.out.println("Successfully created directory: " + directory);
                    } else {
                        System.out.println("Failed to create directory. See server's reply.");
                    }
                } else {
                    result = true;
                    System.out.println("Directory already exists.");
                }
//                ftpClient.logout();
//                ftpClient.disconnect();
//            }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public static boolean saveFilesFTP(List<File> lstFiles, String directory) {
        boolean result = false;

        if (lstFiles.isEmpty()) {
            return true;
        }

        for (File file : lstFiles) {
            result = saveFileFTP(file, directory);
            if (!result) {
                break;
            }
        }

        return result;
    }

    public static boolean saveFileFTP(File file, String directory) {
        boolean result = false;

        //FTPClient ftpClient = connectServer(server, port, user, pass);
        if (connection.isConnected()) {
            try {
//                if (connection != null) {
                if (file != null) {
                    connection.setFileType(FTP.BINARY_FILE_TYPE, FTP.BINARY_FILE_TYPE);
                    connection.setFileTransferMode(FTP.BINARY_FILE_TYPE);
                    InputStream inputStream = new FileInputStream(file);

                    String remotePath = directory + "\\" + file.getName();

                    System.out.println("Start uploading file");
                    OutputStream outputStream = connection.storeFileStream(remotePath);
                    byte[] bytesIn = new byte[4096];
                    int read = 0;

                    while ((read = inputStream.read(bytesIn)) != -1) {
                        outputStream.write(bytesIn, 0, read);
                    }
                    inputStream.close();
                    outputStream.close();

                    result = connection.completePendingCommand();
                    if (result) {
                        System.out.println("The file was uploaded successfully.");
                    }
                }else{
                    result = true;
                }
//                }
            } catch (Exception e) {
                e.printStackTrace();
//            } finally {
//                try {
//                    ftpClient.logout();
//                    ftpClient.disconnect();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
            }
        }

        return result;
    }

    public static boolean saveFiles(List<File> lstFiles, File directory) {
        boolean result = false;

        for (File file : lstFiles) {
            result = saveFile(file, directory);
            if (!result) {
                break;
            }
        }

        return result;
    }

    public static boolean saveFile(File file, File directory) {
        boolean result = false;
        String destinationPath = Paths.get(directory.getAbsolutePath()) + "\\" + file.getName();

        try (FileChannel sourceChannel = new FileInputStream(file).getChannel();
                FileChannel targetChannel = new FileOutputStream(new File(destinationPath)).getChannel()) {
            targetChannel.transferFrom(sourceChannel, 0, sourceChannel.size());

            targetChannel.close();
            sourceChannel.close();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public static String getExtension(File file) {
        return getExtension(file.getAbsolutePath());
    }

    public static String getExtension(String path) {
        String extension = "";
        if (path != null) {
            if (!path.isEmpty()) {

                int i = path.lastIndexOf('.');
                if (i > 0) {
                    extension = path.substring(i + 1);
                }
            }
        }

        return extension;
    }

    public static BufferedImage toImage(InputStream inputStream) {
        BufferedImage bufferedImage = null;

        try {
            File file = new File("temp");

            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file));
            byte[] bytesArray = new byte[4096];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(bytesArray)) != -1) {
                outputStream.write(bytesArray, 0, bytesRead);
            }

            bufferedImage = ImageIO.read(inputStream);
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bufferedImage;
    }

    public static BufferedImage bitmapToImage(InputStream is, String format) {
        BufferedImage image = null;

        try {
            ImageReader rdr = ImageIO.getImageReadersByFormatName(format).next();
            ImageInputStream imageInput = ImageIO.createImageInputStream(is);
            rdr.setInput(imageInput);
            image = rdr.read(0);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return image;
    }

    public static boolean areNotNull(List<File> lstFiles) {
        for (File objFile : lstFiles) {
            if (objFile == null) {
                return false;
            }
        }

        return true;
    }
}
