package com.smartech.sistemacooperativa.util.generics;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class StringUtil {

    private StringUtil() {
    }

    public static String getOneParagraph(List<String> lstLines) {
        String paragraph = "";

        int size = lstLines.size();
        for (int i = 0; i < size; i++) {
            paragraph = paragraph.concat(lstLines.get(i));
            if (i < size - 1) {
                paragraph = paragraph.concat("\n");
            }
        }

        return paragraph;
    }
    
    public static String getCharacters(char character, int quantity){
        String result = "";

        for (int i = 0; i < quantity; i++) {
            result += character;
        }

        return result;
    }

    public static String getAsterisks(int quantity) {
        return getCharacters('*', quantity);
    }

    public static String deleteLastIndex(String string) {
        return deleteLastIndexes(string, 1);
    }

    public static String deleteLastIndexes(String string, int indexes) {
        return string.substring(0, string.length() - indexes);
    }
    
    public static String deleteIndex(String string, int index){
        return new StringBuilder(string).deleteCharAt(index).insert(index, "").toString();
    }

    public static String getStringCode(long code, int size) {
        String codeString = String.valueOf(code);
        int codeStringSize = codeString.length();

        int difference = size - codeStringSize;

        String resultCode = "";
        for (int i = 0; i < difference; i++) {
            resultCode += "0";
        }
        resultCode += codeString;
        
        return resultCode;
    }
    
    public static String encodeString(String string){
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(string.getBytes(StandardCharsets.UTF_8) );
    }
    
    public static String decodeString(String string){
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] decodedByteArray = decoder.decode(string);
 
        return new String(decodedByteArray);
    }
}
