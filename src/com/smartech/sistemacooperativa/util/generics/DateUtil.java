package com.smartech.sistemacooperativa.util.generics;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DateUtil {

    public static SimpleDateFormat sdf = new SimpleDateFormat();

    private DateUtil() {
    }

    public static Timestamp getTimestamp(Date date) {
        return new Timestamp(date.getTime());
    }

    public static Timestamp getTimestamp(Calendar calendar) {
        return getTimestamp(calendar.getTime());
    }

    public static Date getDate(Timestamp timestamp) {
        return new Date(timestamp.getTime());
    }

    public static Calendar getCalendar(Timestamp timestamp) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(getDate(timestamp));
        return calendar;
    }

    public static Calendar getCalendar(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar;
    }

    public static Timestamp currentTimestamp() {
        return new Timestamp(new Date().getTime());
    }

    public static Timestamp setCurrentTime(Timestamp objTimestamp) {
        objTimestamp.setHours(0);
        objTimestamp.setMinutes(0);
        objTimestamp.setSeconds(0);
        objTimestamp.setNanos(0);

        GregorianCalendar objCalendar = new GregorianCalendar();
        objCalendar.set(GregorianCalendar.MILLISECOND, 0);
        objCalendar.set(GregorianCalendar.SECOND, 0);
        objCalendar.set(GregorianCalendar.MINUTE, 0);
        objCalendar.set(GregorianCalendar.HOUR, 0);
        long difference = new GregorianCalendar().getTimeInMillis() - objCalendar.getTimeInMillis();

        objTimestamp.setTime(objTimestamp.getTime() + difference);
        return objTimestamp;
    }

    public static Date zeroTime(Date fecha) {
        Calendar objCalendar = new GregorianCalendar();
        objCalendar.setTime(fecha);
        objCalendar = zeroTime(objCalendar);
        return objCalendar.getTime();
    }

    public static Timestamp zeroTime(Timestamp fecha) {
        Calendar objCalendar = new GregorianCalendar();
        objCalendar.setTime(new Date(fecha.getTime()));
        objCalendar = zeroTime(objCalendar);
        return new Timestamp(objCalendar.getTime().getTime());
    }

    public static Calendar zeroTime(Calendar fecha) {
        fecha.set(Calendar.HOUR_OF_DAY, 0);
        fecha.set(Calendar.MINUTE, 0);
        fecha.set(Calendar.SECOND, 0);
        fecha.set(Calendar.MILLISECOND, 0);
        return fecha;
    }

    public static Timestamp zeroMonthTime(Timestamp fecha) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(fecha);
        return zeroMonthTime(calendar);
    }

    public static Timestamp zeroMonthTime(Calendar fecha) {
        fecha.set(Calendar.DAY_OF_MONTH, 1);
        return zeroTime(new Timestamp(fecha.getTime().getTime()));
    }

    public static Timestamp lastMonthTime(Timestamp fecha) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(fecha);
        return lastMonthTime(calendar);
    }

    public static Timestamp lastMonthTime(Calendar fecha) {
        fecha.set(Calendar.DAY_OF_MONTH, 1);
        fecha.add(Calendar.MONTH, 1);
        fecha.add(Calendar.DAY_OF_MONTH, -1);
        return lastTime(new Timestamp(fecha.getTime().getTime()));
    }

    public static Date lastTime(Date fecha) {
        Calendar objCalendar = new GregorianCalendar();
        objCalendar.setTime(fecha);
        objCalendar = lastTime(objCalendar);
        return objCalendar.getTime();
    }

    public static Timestamp lastTime(Timestamp fecha) {
        Calendar objCalendar = new GregorianCalendar();
        objCalendar.setTime(new Date(fecha.getTime()));
        objCalendar = lastTime(objCalendar);
        return new Timestamp(objCalendar.getTime().getTime());
    }

    public static Calendar lastTime(Calendar fecha) {
        fecha.set(Calendar.HOUR_OF_DAY, 23);
        fecha.set(Calendar.MINUTE, 59);
        fecha.set(Calendar.SECOND, 59);
        fecha.set(Calendar.MILLISECOND, 999);
        return fecha;
    }

    public static Timestamp add(Timestamp timestamp, int field, int ammount) {
        Calendar calendar = getCalendar(timestamp);
        calendar.add(field, ammount);

        return getTimestamp(calendar);
    }

    public static Timestamp toTimestamp(Calendar objCalendar) {
        return new Timestamp(objCalendar.getTime().getTime());
    }

    public static Timestamp toTimestamp(Date fecha) {
        return new Timestamp(fecha.getTime());
    }
    
    public static String getRegularTime(Date objDate){
        return getDate(objDate, "HH:mm:ss");
    }

    public static String getRegularDateTime(Date objDate) {
        return getDate(objDate, "dd-MM-yyyy HH:mm");
    }

    public static String getRegularDate(Date objDate) {
        return getDate(objDate, "dd-MM-yyyy");
    }

    public static String getDate(Date objDate, String pattern) {
        sdf.applyPattern(pattern);
        return sdf.format(objDate);
    }

    public static int getYearsDifference(Date startDate, Date finishDate) {
        if (startDate.before(finishDate)) {
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(startDate);
            int startYear = calendar.get(Calendar.YEAR);
            int startMonth = calendar.get(Calendar.MONTH);
            int startDay = calendar.get(Calendar.DAY_OF_MONTH);

            calendar.setTime(finishDate);
            int years = calendar.get(Calendar.YEAR) - startYear;
            int finishMonth = calendar.get(Calendar.MONTH);
            int finishDay = calendar.get(Calendar.DAY_OF_MONTH);

            if (finishMonth == startMonth) {
                return finishDay >= startDay ? years : years - 1;
            } else {
                return finishMonth > startMonth ? years : years - 1;
            }
        } else {
            return 0;
        }
    }

    public static int getMonthsDifference(Date startDate, Date finishDate) {
        if (startDate.before(finishDate)) {
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(startDate);
            int startYear = calendar.get(Calendar.YEAR);
            int startMonth = calendar.get(Calendar.MONTH);
            int startDay = calendar.get(Calendar.DAY_OF_MONTH);

            calendar.setTime(finishDate);
            int years = calendar.get(Calendar.YEAR) - startYear;
            int months = (years * 12) + calendar.get(Calendar.MONTH) - startMonth;
            int finishDay = calendar.get(Calendar.DAY_OF_MONTH);

            return finishDay <= startDay ? months - 1 : months;
        } else {
            return 0;
        }
    }

    public static int getDaysDifference(Timestamp startTimestamp, Timestamp finishTimestamp) {
        return getDaysDifference(getDate(startTimestamp), getDate(finishTimestamp));
    }

    public static int getDaysDifference(Date startDate, Date finishDate) {
        if (startDate.before(finishDate)) {
            Long diferencia = (finishDate.getTime() - startDate.getTime()) / (24 * 60 * 60 * 1000);

            return diferencia.intValue();
        } else {
            return 0;
        }
    }

    public static List<Date> getStartFinishYearDates(Date date) {
        List<Date> lstDates = new ArrayList<>();

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);

        calendar.set(calendar.get(Calendar.YEAR), 0, 1, 0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        lstDates.add(calendar.getTime());

        calendar.add(Calendar.YEAR, 1);
        calendar.add(Calendar.MILLISECOND, -1);
        lstDates.add(calendar.getTime());

        return lstDates;
    }

    public static List<Date> getStartFinishMonthDates(Date date) {
        List<Date> lstDates = new ArrayList<>();

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);

        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1, 0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        lstDates.add(calendar.getTime());

        calendar.add(Calendar.MONTH, 1);
        calendar.add(Calendar.MILLISECOND, -1);
        lstDates.add(calendar.getTime());

        return lstDates;
    }

    public static List<Date> getStartFinishFirstWeekDates(Date date) {
        List<Date> lstDates = new ArrayList<>();

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);

        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1, 0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        lstDates.add(calendar.getTime());

        calendar.add(Calendar.WEEK_OF_MONTH, 1);
        calendar.add(Calendar.MILLISECOND, -1);
        lstDates.add(calendar.getTime());

        return lstDates;
    }

    public static List<Date> getStartFinishDates(Date date, int daysDifference) {
        List<Date> lstDates = new ArrayList<>();

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);

        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1, 0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        lstDates.add(calendar.getTime());

        calendar.add(Calendar.DAY_OF_MONTH, daysDifference);
        calendar.add(Calendar.MILLISECOND, -1);
        lstDates.add(calendar.getTime());

        return lstDates;
    }
    
    public static long getMinutesDifference(Timestamp startTimestamp, Timestamp finishTimestamp){
        return getMinutesDifference(getCalendar(startTimestamp), getCalendar(finishTimestamp));
    }

    public static long getMinutesDifference(Calendar startCalendar, Calendar finishCalendar) {
        if (startCalendar.before(finishCalendar)) {
            Long diferencia = (finishCalendar.getTime().getTime() - startCalendar.getTime().getTime()) / (60 * 1000);

            return diferencia;
        } else {
            return 0;
        }
    }
    
    public static boolean hasDay(int day, Calendar calendar){
        Calendar compareCalendar = Calendar.getInstance();
        Calendar originalCalendar = Calendar.getInstance();
        originalCalendar.setTime(calendar.getTime());
        compareCalendar.setTime(getCalendar(lastMonthTime(originalCalendar)).getTime());
        
        return compareCalendar.get(Calendar.DAY_OF_MONTH) >= calendar.get(Calendar.DAY_OF_MONTH);
    }
}
